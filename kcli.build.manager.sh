#!/bin/bash

BUILD_NUMBER=$1
BUILDS_TO_KEEP=$2
PROPERTIES_FILE=$3
SNAPSHOT=$4

eval "sed 's/klab.product.builds=/klab.product.builds=${BUILD_NUMBER},/' $3" >> tmp.prop && mv tmp.prop $3

BUILD_LIST=$(grep 'klab.product.builds' $3)
IFS=', ' read -r -a array <<< "$BUILD_LIST"
NEW_BUILD_LIST=$(printf ",%s" "${array[@]:0:$2}")
NEW_BUILD_STRING=${NEW_BUILD_LIST:1}

for build in "${array[@]:$2}"; do
    rm -rf "$build"
done

eval "sed 's/klab.product.builds=.*/${NEW_BUILD_STRING}/' $3" >> tmp.prop && mv tmp.prop $3

eval "mkdir ${BUILD_NUMBER}"

mv cli-*.jar "${BUILD_NUMBER}/"
mv lib "${BUILD_NUMBER}"

cd $BUILD_NUMBER

md5sum `find . -type f -print` > filelist.txt
date=$(date '+%Y-%m-%dT%H:%M:%S')
touch build.properties
echo "klab.product.build.version=${SNAPSHOT}" >>  build.properties
echo "klab.product.build.time=${date}" >>  build.properties
echo "klab.product.build.main=org.integratedmodelling.klab.k.Main" >> build.properties
#rm lib/javax.annotation-1.2.0.v201602091430.jar this should no longer end up in the lib folder

#!/bin/bash

BUILD_NUMBER=$1
BUILDS_TO_KEEP=$2

rm -f latest

for build in "${array[@]:$2}"; do
    rm -rf "$build"
done

eval "mkdir ${BUILD_NUMBER}"

find . -maxdepth 1 -type f ! -name "kcontrolcenter.build.manager.sh" -exec mv {} $BUILD_NUMBER \;

ln -s $BUILD_NUMBER latest

for FILE in klab_controlcenter*; do ln -s $FILE "klab${FILE#*controlcenter*}"; done;

#!/bin/bash

BUILD_NUMBER=$1
BUILDS_TO_KEEP=$2
PROPERTIES_FILE=$3
SNAPSHOT=$4

eval "sed 's/klab.product.builds=/klab.product.builds=${BUILD_NUMBER},/' $3" >> tmp.prop && mv tmp.prop $3

BUILD_LIST=$(grep 'klab.product.builds' $3)
IFS=', ' read -r -a array <<< "$BUILD_LIST"
NEW_BUILD_LIST=$(printf ",%s" "${array[@]:0:$2}")
NEW_BUILD_STRING=${NEW_BUILD_LIST:1}

for build in "${array[@]:$2}"; do
    rm -rf "$build"
done

eval "sed 's/klab.product.builds=.*/${NEW_BUILD_STRING}/' $3" >> tmp.prop && mv tmp.prop $3

eval "mkdir ${BUILD_NUMBER}"

distros=("unix" "win" "macos")

distros_zip=("org.integratedmodelling.klab.modeler.product-linux.gtk.x86_64.zip" "org.integratedmodelling.klab.modeler.product-win32.win32.x86_64.zip" "org.integratedmodelling.klab.modeler.product-macosx.cocoa.x86_64.zip")

for i in {0..2}; do
    echo "$PWD"
    mkdir  "${PWD}/${BUILD_NUMBER}/${distros[i]}"
    unzip "${PWD}/${distros_zip[i]}" -d "${PWD}/${BUILD_NUMBER}/${distros[i]}"
    rm -f "${distros_zip[i]}"
done

for i in {0..1}; do
    mv "${PWD}/${BUILD_NUMBER}/${distros[i]}/kmodeler"/* "${PWD}/${BUILD_NUMBER}/${distros[i]}/"
    rm -r "${PWD}/${BUILD_NUMBER}/${distros[i]}/kmodeler"
    cd "${PWD}/${BUILD_NUMBER}/${distros[i]}/"
    md5sum `find . -type f -print` > filelist.txt
    cd "../.."
done

cd "${PWD}/${BUILD_NUMBER}/${distros[2]}/"
ls .
md5sum `find . -type f -print` > filelist.txt

cd ".."
date=$(date '+%Y-%m-%dT%H:%M:%S')
touch build.properties
echo "klab.product.build.version=${SNAPSHOT}" >>  build.properties
echo "klab.product.build.time=${date}" >>  build.properties